# Halium config for moto g⁷ play

## Progress

|         Component | Status | Details            |
|------------------:|:------:|--------------------|
|      SSH over USB |        |                    |
|          AppArmor |        |                    |
|      Boot into UI |        |                    |
|            Camera |        |                    |
|    Cellular Calls |        |                    |
|     Cellular Data |        |                    |
|               GPS |        |                    |
|           Sensors |        |                    |
|             Sound |        |                    |
| UBPorts Installer |        |                    |
|  UBPorts Recovery |        |                    |
|          Vibrator |        |                    |
|             Wi-Fi |        |                    |

## Specs

Basic   | Spec Sheet
-------:|:-------------------------
CPU     | Octa-core 1.8 GHz Cortex-A53
CHIPSET | Qualcomm SDM632 Snapdragon 632
GPU     | Adreno 506
Memory  | 2GB
Shipped Android Version | 9.0 (Pie)
Internal Storage | 32GB
microSD | Up to 1 TB (dedicated slot)
Battery | 3000 mAh
Dimensions | 147.3 x 71.5 x 8 mm 
Display | 720 x 1512  pixels, 5.7-inch IPS LCD
Rear Camera  | 13 MP (f/2.0, 1.12µm, PDAF)
Front Camera | 8 MP (f/2.2, 1.12µm, HDR)

## Pretty picture

![Motorola g7 play](https://fdn2.gsmarena.com/vv/pics/motorola/motorola-moto-g7-play-1.jpg "Motorola g7 play")
